#include <SerialFlash.h>
#include <stdint.h>
#include "flash_simple_api.h"
#include "config_structs.h"
#include "config.h"

extern SerialFlashChip SerialFlash;

#if USE_MOCK_FLASH == 1
Gateway_Cfg gwc;
Node_Gateway_Cfg noc = {13,12,0,50,"AAAABBBBCCCC",20,"0987654321234567","Depot","MainNode","T1","S1","T2","S2","T3","S3","T4","S4"};
Node_Internal_Cfg nic = {0,20,50,12,25,"0987654321234567"};
#endif

uint8_t gateway_config_file_create(Gateway_Cfg * cfg_str) {
#if USE_MOCK_FLASH == 1
  memcpy((uint8_t *)&gwc, (uint8_t *)cfg_str, sizeof(Gateway_Cfg));
  return CFG_OK;
#else
  if (SerialFlash.exists("gw_config.bin")) {
    return CFG_EXISTS_ALREADY;
  } else {
    if (!SerialFlash.createErasable("gw_config.bin", sizeof(Gateway_Cfg))) {
      return CFG_NO_SPACE; /* No Space */
    }
  }

  SerialFlashFile file;
  file = SerialFlash.open("gw_config.bin");
  uint8_t * data_ptr;
  data_ptr = (uint8_t*)cfg_str;
  if (file) {
    uint8_t res;
    res = file.write(data_ptr, sizeof(Gateway_Cfg));
    file.close();
    if (res != 0) {
      return CFG_OK;
    } else {
      return CFG_WRITE_ERR;
    }
  } else {
    return CFG_OPEN_ERR;
  }
#endif
}


uint8_t gateway_config_file_read(Gateway_Cfg * cfg_str) {
#if USE_MOCK_FLASH == 1
  cfg_str->port_mqtt = 1883;
  cfg_str->gateway_id = 12;
  cfg_str->power = 10;
  strcpy_P(cfg_str->gateway_location,PSTR("LakeHouse"));
  strcpy_P(cfg_str->gateway_name,PSTR("MainGW"));
  strcpy_P(cfg_str->user_mqtt,PSTR(""));
  strcpy_P(cfg_str->pass_mqtt,PSTR(""));
  strcpy_P(cfg_str->host_mqtt,PSTR("127.0.0.1"));
  strcpy_P(cfg_str->crypto_key,PSTR("1234567890123456"));
  return CFG_OK;
#else
  if (!SerialFlash.exists("gw_config.bin")) {
    /* file doesn't exist */
    return CFG_DOESNT_EXIST;
  } else {
    /* config file exists */
    SerialFlashFile file;
    file = SerialFlash.open("gw_config.bin");
    if (file) {
      uint8_t * data_ptr;
      data_ptr = (uint8_t*)cfg_str;
      uint8_t res;
      res = file.read(data_ptr, sizeof(Gateway_Cfg));
      file.close();
      if (res != 0) {
        return CFG_OK;
      } else {
        return CFG_READ_ERR;
      }
    } else {
      return CFG_OPEN_ERR;
    }
  }

#endif
}

/* Updates the gateway config file. If it doesn't exist, it will be created */
uint8_t gateway_config_file_update(Gateway_Cfg * cfg_str) {
#if USE_MOCK_FLASH == 1
  memcpy((uint8_t *)&gwc, (uint8_t *)cfg_str, sizeof(Gateway_Cfg));
  return CFG_OK;
#else
  if (!SerialFlash.exists("gw_config.bin")) {
    /* File doesn't exist */
    if (!SerialFlash.createErasable("gw_config.bin", sizeof(Gateway_Cfg))) {
      return CFG_NO_SPACE; /* No Space */
    }
  }

    /* config file exists */
    SerialFlashFile file;
    file = SerialFlash.open("gw_config.bin");
    if (file) {
      file.erase();
      file.seek(0);
      uint8_t * data_ptr;
      data_ptr = (uint8_t*)cfg_str;
      uint8_t res;
      res = file.write(data_ptr, sizeof(Gateway_Cfg));
      //file.close();
      if (res != 0) {
        return CFG_OK;
      } else {
        return CFG_WRITE_ERR;
      }
    } else {
      return CFG_OPEN_ERR;
    }
#endif
}


uint8_t backup_file_create(char * name_f, Sensor_Data * bkp_str) {
#if USE_MOCK_FLASH == 1
  return CFG_OK;
#else
  if (SerialFlash.exists(name_f)) {
    return BKP_EXISTS_ALREADY;
  } else {
    if (!SerialFlash.createErasable((const char *)name_f, sizeof(Sensor_Data))) {
      return BKP_NO_SPACE; /* No Space */
    }
  }

  SerialFlashFile file;
  file = SerialFlash.open(name_f);
  uint8_t * data_ptr;
  data_ptr = (uint8_t*)bkp_str;
  if (file) {
    uint8_t res;
    res = file.write(data_ptr, sizeof(Sensor_Data));
    // file.close();

    if (res != 0) {
      return BKP_OK;
    } else {
      return BKP_WRITE_ERR;
    }
  } else {
    return BKP_OPEN_ERR;
  }
#endif
}

uint8_t backup_file_read(char * name, Sensor_Data * bkp_str) {
  if (!SerialFlash.exists(name)) {
    /* file doesn't exist */
    return BKP_DOESNT_EXIST;
  } else {
    /* config file exists */
    SerialFlashFile file;
    file = SerialFlash.open(name);
    if (file) {
      uint8_t * data_ptr;
      data_ptr = (uint8_t*)bkp_str;
      uint8_t res;
      res = file.read(data_ptr, sizeof(Sensor_Data));
      file.close();
      if (res != 0) {
        return BKP_OK;
      } else {
        return BKP_READ_ERR;
      }
    } else {
      return BKP_OPEN_ERR;
    }
  }
}

uint8_t backup_file_delete(char * name) {
  if (!SerialFlash.exists(name)) {
    /* file doesn't exist */
    return BKP_DOESNT_EXIST;
  } else {
    if (SerialFlash.remove(name)) {
      return BKP_OK;
    } else {
      return BKP_DELETE_ERR;
    }
  }
}

uint8_t gateway_node_config_update(Node_Gateway_Cfg * node_cfg){
#if USE_MOCK_FLASH == 1
  memcpy((uint8_t *)&noc, (uint8_t *)node_cfg, sizeof(Node_Gateway_Cfg));
  return CFG_OK;
#else
  char node_filename[20];
  sprintf(node_filename,"node%d.bin",node_cfg->node_id);
  
  if (SerialFlash.exists(node_filename)) {
  } else {
    if (!SerialFlash.createErasable(node_filename, sizeof(Node_Gateway_Cfg))) {
      return CFG_WRITE_ERR;
    }
  }

  SerialFlashFile file;
  file = SerialFlash.open(node_filename);
  uint8_t * data_ptr;
  data_ptr = (uint8_t*)node_cfg;
  if (file) {
    uint8_t res;
    file.erase();
    file.seek(0); 
    res = file.write(data_ptr, sizeof(Node_Gateway_Cfg));
    file.close();
    if (res != 0) {
      return CFG_OK;
    } else {
      return CFG_WRITE_ERR;
    }
  } else {
    return CFG_OPEN_ERR;
  }
#endif
}

uint8_t gateway_node_config_read(uint8_t node_id, Node_Gateway_Cfg * cfg_str){
  #if USE_MOCK_FLASH == 1
    memcpy((uint8_t *)cfg_str, (uint8_t *)&noc, sizeof(Node_Gateway_Cfg));
    return CFG_OK;
  #else
  char node_filename[20];
  sprintf(node_filename,"node%d.bin",node_id);

  if (!SerialFlash.exists(node_filename)) {
    return CFG_DOESNT_EXIST;
  }

  /* config file exists */
  SerialFlashFile file;
  file = SerialFlash.open(node_filename);
  if (file) {
    uint8_t * data_ptr;
    data_ptr = (uint8_t*)cfg_str;
    uint8_t res;
    res = file.read(data_ptr, sizeof(Node_Gateway_Cfg));
    file.close();
    if (res != 0) {
      return CFG_OK;
    } else {
      return CFG_READ_ERR;
    }
  } else {
    return CFG_OPEN_ERR;
  }

  #endif
}

uint8_t node_internal_config_update(Node_Internal_Cfg* node_cfg) {
#if USE_MOCK_FLASH == 1
  memcpy((uint8_t *)&nic, (uint8_t *)node_cfg, sizeof(Node_Internal_Cfg));
  return CFG_OK;
#else
  if (SerialFlash.exists("config.bin")) {
  } else {
    return CFG_DOESNT_EXIST;
  }

  SerialFlashFile file;
  file = SerialFlash.open("config.bin");
  uint8_t * data_ptr;
  data_ptr = (uint8_t*)node_cfg;
  if (file) {
    uint8_t res;
    file.erase();
    file.seek(0); 
    res = file.write(data_ptr, sizeof(Node_Internal_Cfg));
    file.close();
    if (res != 0) {
      return CFG_OK;
    } else {
      return CFG_WRITE_ERR;
    }
  } else {
    return CFG_OPEN_ERR;
  }
#endif
}

uint8_t node_internal_config_create(Node_Internal_Cfg* node_cfg) {
  
  if (SerialFlash.exists("config.bin")) {
  } else {
    if (!SerialFlash.createErasable("config.bin", sizeof(Node_Internal_Cfg))) {
       return CFG_WRITE_ERR;
    }
   
  }

  SerialFlashFile file;
  file = SerialFlash.open("config.bin");
  uint8_t * data_ptr;
  data_ptr = (uint8_t*)node_cfg;
  if (file) {
    uint8_t res;
    file.erase();
    file.seek(0); 
    res = file.write(data_ptr, sizeof(Node_Internal_Cfg));
    file.close();
    if (res != 0) {
      return CFG_OK;
    } else {
      return CFG_WRITE_ERR;
    }
  } else {
    return CFG_OPEN_ERR;
  }
}

uint8_t node_internal_config_read(Node_Internal_Cfg * cfg_str) {

#if USE_MOCK_FLASH == 1
  memcpy((uint8_t *)cfg_str, (uint8_t *)&nic, sizeof(Node_Internal_Cfg));
  return CFG_OK;
#else

  if (!SerialFlash.exists("config.bin")) {
    /* file doesn't exist */
    return CFG_DOESNT_EXIST;
  } else {
    /* config file exists */
    SerialFlashFile file;
    file = SerialFlash.open("config.bin");
    if (file) {
      uint8_t * data_ptr;
      data_ptr = (uint8_t*)cfg_str;
      uint8_t res;
      res = file.read(data_ptr, sizeof(Node_Internal_Cfg));
      file.close();
      if (res != 0) {
        return CFG_OK;
      } else {
        return CFG_READ_ERR;
      }
    } else {
      return CFG_OPEN_ERR;
    }
  }

#endif
}

uint8_t node_internal_config_check(void){
#if USE_MOCK_FLASH == 1
    return 0;
#else    
    bool res;
    res = SerialFlash.exists("config.bin");
    if(res == true){
      return 0;  /* config.bin exists */
    }else{
      return 1; /* config.bin does not exist */
    }
#endif

}

uint8_t flash_init(void){
  #if USE_MOCK_FLASH == 1
    return 0;
  #else
  bool res = SerialFlash.begin(FLASH_CS_PIN);
  if(res){
    return 0; /* OK */
  }else{
    return 1;
  }
  #endif
}

uint8_t flash_ready(void){
  #if USE_MOCK_FLASH == 1
    return 0;
  #else
  bool res = SerialFlash.ready();
  if(res){
    return 0; /* OK */
  }else{
    return 1;
  }
  #endif
}
void flash_wakeup(void){
  #if USE_MOCK_FLASH == 1
    return;
  #else
    SerialFlash.wakeup();
  #endif
}

void flash_erase_all(void){
  #if USE_MOCK_FLASH == 1
    return;
  #else
    SerialFlash.eraseAll();
  #endif
}

uint8_t flash_readdir(char * filename, uint32_t strsize, uint32_t &filesize){
  #if USE_MOCK_FLASH == 1
    return 1;
  #else
  bool res = SerialFlash.readdir(filename, strsize, filesize);
  if(res){
    return 0; /* file Exists */
  }else{
    return 1;
  }
  #endif
}

void flash_opendir(void){
  #if USE_MOCK_FLASH == 1
    return;
  #else
    SerialFlash.opendir();
  #endif
}

uint8_t backup_file_check(const char * name){
  #if USE_MOCK_FLASH == 1
    return 1;
  #else
  bool res = SerialFlash.exists(name);
  if(res){
    return 0; /* file Exists */
  }else{
    return 1;
  }
  #endif
}

void flash_delete_all_backup_files(Node_Internal_Cfg * ncfg)
{
  #if USE_MOCK_FLASH == 1
    return;
  #else
    SerialFlash.eraseAll();
    while (SerialFlash.ready() == false)
    {
      delay(1000);
    }
    node_internal_config_create(ncfg);
  #endif
}

void flash_sleep(void){
  #if USE_MOCK_FLASH == 1
    return;
  #else
    SerialFlash.sleep();
  #endif
}