#include <Wire.h>
#include "config_structs.h"
#include "flash_simple_api.h"
#include "device_id.h"
#include "rtc.h"
#include "sleep.h"
#include "comm.h"
#include "config.h"
#include "sensor_base.h"

Node_Internal_Cfg node_config;

void setup()
{
  pinMode(RF95_CSS_PIN, OUTPUT); /* Before starting flash. */
  digitalWrite(RF95_CSS_PIN, HIGH);

  delay(500);

  pinMode(INTERRUPT_PIN, INPUT_PULLUP);

  rtc_begin();
  sensor_base_init();

  Serial.begin(115200);

  Serial.println(F("Starting Flash Memory.."));
  if (flash_init())
  {
    Serial.println(F("Error flash memory communication!!!. Halting."));
    while (1){}
  }

  /* Wakeup flash in case it's sleeping */
  flash_wakeup();
  while (flash_ready()){}
  delay(1000);

  /* Check serial port for initial config data */
  Node_Internal_Cfg initial_config;

  uint8_t res = 0;

  /* Check if config file exist */
  if (node_internal_config_check())
  {
    /* Config doesn't exists. Wait for initial serial config indefinitely */
    while (config_binary_via_serial(&initial_config))
    {
    }
    /* Serial config was received. Create the file */
    Serial.println(F("Creating File"));
    res = node_internal_config_create(&initial_config);
    /* Check for errors */
    if (res == CFG_NO_SPACE)
    {
      Serial.println(F("No space. Deleting backup files"));
      node_config = initial_config;
      flash_delete_all_backup_files(&node_config);
    }
    else if (res != CFG_OK)
    {
      Serial.println(F("Error creating config file. Halting..."));
      while (1)
        ;
    }
    else
    {
      /* Everything went ok */
      Serial.print(F("Config file created Correctly"));
    }
  }
  else
  {
    /* Config file exists */
    /* Try to update config via serial once */
    if (config_binary_via_serial(&initial_config))
    {
      /* No response via serial */
      Serial.println(F("Serial Config. no response."));
    }
    else
    {
      /* Response received. Update the config file */
      Serial.println(F("Updating File"));
      res = node_internal_config_update(&initial_config);
      if (res != CFG_OK)
      {
        /* Fatal error */
        Serial.println(F("Error updating config file. Halting..."));
        while (1){}
          
      }
    }
  }

  /* End initial serial config ---------------------------------------- */

  /* Read the config file from flash memory */
  Serial.println(F("Reading config from flash"));
  res = node_internal_config_read(&initial_config);
  Serial.println(F("Pass read"));
  if (res != CFG_OK)
  {
    Serial.print(F("Error while reading the config. file from flash, #"));
    Serial.println(res);
    Serial.println(F("Halting"));
    while (1)
      ;
  }

  /* initial_config has at least the node_id and the gw_id */

  node_config = initial_config;
  Serial.println(F("\nStarting Configuration"));
  config_print(&node_config); /* Printing the config to serial */
  Serial.println();


  /* Start radio manager object */
  Serial.println(F("Starting RF95.."));
  if (rf_init())
  {
    Serial.println(F("Error RH_RF95 init failed"));
    while (1){}
  }

  rf_set_frequency(915.0);
  rf_set_power(node_config.power);
  rf_set_address(node_config.node_id);

  /* Send a request for config data before start measuring */
  
  // while(rf_ask_for_auth()){
  //    /*Wait indefinitely */
  //   delay(1000);
  // }

  rf_ask_for_config(&node_config);

  /* Enable the Interrupt */
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), config_interrupt_binary_via_serial, FALLING);

  if (node_internal_config_read(&node_config))
  {
    /* Error */
    Serial.println(F("Error. Can't read config file. Halting the system..."));
    while (1){}
  }

  Serial.println(F("Setup Done"));
}

void loop()
{
  Sensor_Data data;
  Sensor_Data old_data;

  /* Flag to mark if the current measure was stored in flash or not */
  uint8_t current_data_saved_flg = 0;

  uint32_t loop_start_time;
  loop_start_time = millis();

  /* Print the current config (for debugging purposes). */
  Serial.println(F("\nCurrent Node Config: "));
  config_print(&node_config);
  Serial.println();
  flash_wakeup();

  /* -------- Read the sensors -----------*/
  //tc.read(); /* Read thermopar */
  //data.thermopar_data = tc.getInternal();
  data.rssi = rf_rssi();             /* get RSSI from RF driver */
  data.battery_level = sbase_battery(); /* Read battery level */

  /* Timestamp using the RTC */
  sprintf(data.timestamp, "%02d-%02d-%02d %02d:%02d:%02d", rtc_year(), rtc_month(), rtc_day(), rtc_hour(), rtc_minute(), rtc_second());
  memset(data.p1, 0, 20);
  memset(data.p2, 0, 20);
  memset(data.p3, 0, 20);
  memset(data.p4, 0, 20);
  strcpy(data.p1, "111");
  strcpy(data.p2, "222");
  strcpy(data.p3, "333");
  strcpy(data.p4, "444");

  /* Check for Existing Data in Flash */

  /* List files in flash */
  flash_opendir();
  char filename[20];
  uint32_t filesize;

  
  while (!flash_readdir(filename, sizeof(filename), filesize))
  {
    int res;
    res = strcmp("config.bin", filename);
    if (res == 0)
    {
      /* The file is the config file. Ignore it. */
      continue;
    }

    char *ret_ptr;
    ret_ptr = strstr(filename, "bkp");
    if (ret_ptr == NULL)
    {
      /* Not a backup file. Ignore it. */
      continue;
    }

    /* Backup file */
    res = backup_file_read(filename, &old_data);
    if (res != BKP_OK)
    {
      /* Error reading this file */
      continue;
    }

    /* Try to send */
    if (!rf_send_sensor_data(&old_data,&node_config))
    {
      /* Communication Succeded. Delete file. */
      backup_file_delete(filename);
    }
    else
    {
      /* Communication not working.
           Find an available name and save the new data.
        */

      char bkp_filename[10];
      uint16_t file_number = 0;
      /* Propose a filename and check if it exist  */
      sprintf(bkp_filename, "bkp%03d", file_number++);
      while (backup_file_check(bkp_filename) == 0)
      {
        sprintf(bkp_filename, "bkp%03d", file_number++);
      }
      /* if 0 to 999 are used then start from 0 again */
      if (file_number == 1000)
      {
        file_number = 0;
        sprintf(bkp_filename, "bkp%03d", file_number++);
      }
      /* Try to create the file */
      res = backup_file_create(bkp_filename, &data);
      if (res != BKP_OK)
      {
        /* Data will be lost */
        Serial.print(F("Error while saving backup. Error #"));
        Serial.println(res);
        if (res == BKP_NO_SPACE)
        {
          Serial.println(F("No space. Deleting all backup files."));
          flash_delete_all_backup_files(&node_config);
        }
      }
      else
      {
        Serial.println(F("Backup Created"));
      }
      current_data_saved_flg = 1;
      break; /* Break from file list analysis because RF is not working */
    }
  } /* End while */

  if (current_data_saved_flg == 0)
  {
    /* Try to send */
    Serial.println(F("Trying to send"));
    if (!rf_send_sensor_data(&data,&node_config))
    {
      /* Data sent ok */
      Serial.println(F("Data Sent OK"));
      /* Request new config data if available */
      rf_ask_for_config(&node_config);
    }
    else
    {
      Serial.println(F("Data Sent ERROR"));
      /* Fail to send. Save backup. */
      char bkp_filename[10];
      uint16_t file_number = 0;
      uint8_t res;
      sprintf(bkp_filename, "bkp%03d", file_number++);

      while (backup_file_check(bkp_filename) == 0)
      {
        sprintf(bkp_filename, "bkp%03d", file_number++);
      }
      if (file_number == 1000)
      {
        file_number = 0;
        sprintf(bkp_filename, "bkp%03d", file_number++);
      }
      res = backup_file_create(bkp_filename, &data);
      if (res != BKP_OK)
      {
        /* Data will be lost */
        if (res == BKP_NO_SPACE)
        {
          flash_delete_all_backup_files(&node_config);
        }
        Serial.print(F("Error while saving backup. Error "));
        Serial.println(res);
      }
    }
  }

  current_data_saved_flg = 0;

  /* Send flash and radio to sleep */
  flash_sleep();
  rf_sleep();

  uint32_t loop_end_time;
  loop_end_time = millis();
  uint32_t elapsed_time;
  elapsed_time = loop_end_time - loop_start_time;

  /* Send the microcontroller to sleep */
  /* Sleep time in milliseconds */
  int32_t sleep_time_ms;
  sleep_time_ms = (node_config.collection_time * 1000);
  sleep_time_ms -= elapsed_time; /* Discount the time spent in the loop */

  if (sleep_time_ms > 0)
  {
    system_sleep(sleep_time_ms);
  }
}

uint8_t config_binary_via_serial(Node_Internal_Cfg *dc_ptr)
{
  uint8_t res;
  Serial.setTimeout(SERIAL_TIMEOUT_MS);
  Serial.flush();
  Serial.write(0xAA); /* Send the start marker to the computer */

  /* Receive the config. struct */
  res = Serial.readBytes((char *)dc_ptr, sizeof(Node_Internal_Cfg));
  if (res == 0)
  {
    return 1;
  }

  Serial.write(0xBB);
  Serial.println(F("Received data via serial..."));
  config_print(dc_ptr);
  Serial.println();

  delay(1000);

  return 0;
}

/* This function is just a wrapper to use config_binary_via_serial in an interrupt */
void config_interrupt_binary_via_serial(void)
{
  Node_Internal_Cfg dc;
  uint8_t res;
  res = config_binary_via_serial(&dc);
  if (res == 0)
  {
    config_print(&dc);
  }
  else
  {
    Serial.print(F("Error Configuring via serial"));
  }
}

void config_print(Node_Internal_Cfg *dc)
{
  Serial.print(F("\nNodeId: "));
  Serial.println(dc->node_id);
  Serial.print(F("GwId: "));
  Serial.println(dc->gateway_id);
  Serial.print(F("RF Power: "));
  Serial.println(dc->power);
  Serial.print(F("ColectionTime: "));
  Serial.println(dc->collection_time);
  Serial.print(F("Encryption Key: "));
  for (uint8_t z = 0; z < 16; z++)
  {
    Serial.print(dc->crypto_key[z]);
    Serial.print(" ");
  }
  Serial.println();
}

void config_print_rf(Node_RF_Cfg *dc)
{
  Serial.print(F("\nNodeId: "));
  Serial.println(dc->node_id);
  Serial.print(F("GwId: "));
  Serial.println(dc->gateway_id);
  Serial.print(F("RF Power: "));
  Serial.println(dc->power);
  Serial.print(F("ColectionTime: "));
  Serial.println(dc->collection_time);
  Serial.println();
}



void flush_serial(void)
{
  while (Serial.available())
  {
    Serial.read();
  }
}

#define ASSERT(expr) \
  ((expr) ||         \
   aFailed(__LINE__))

static bool aFailed(int line_number)
{
  Serial.print(F("Failed ASSERT line "));
  Serial.println(line_number);
  while (1){}
  return true;
}

static void device_id_get(uint8_t *destino)
{
  const uint8_t dev_id_length = 12;
  const int deviceaddress = 0x50;
  const byte eeaddress = 0xF8;
  Wire.begin();
  Wire.beginTransmission(deviceaddress);
  Wire.write(eeaddress); // LSB
  Wire.endTransmission();
  Wire.requestFrom(deviceaddress, 8); //request 8 bytes from the device
  uint8_t received = 0;
  uint8_t index = 0;
  while (Wire.available() && (received++ < dev_id_length))
  {
    destino[index++] = Wire.read();
  }

  ASSERT(index <= dev_id_length);
}
