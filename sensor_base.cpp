#include "sensor_base.h"
#include "config.h"
#include <Nanoshield_Termopar.h>
#include <Wire.h>

Nanoshield_Termopar tc(THERMO_CS_PIN, TC_TYPE_K, TC_AVG_OFF);

void sensor_base_init(void){
  tc.begin(); /* Initialize Thermopar */
}

int sbase_battery(void)
{
  float battery_voltage = 0;
  int battery_percentage = 0;

  battery_voltage = (analogRead(BATTERY_PIN) * 3300.0) / 4098.0;
  battery_percentage = (battery_voltage - 2900.0) / 11.40;

  if (battery_percentage > 100)
    battery_percentage = 100;

  if (battery_percentage < 1)
    battery_percentage = 1;

  return (battery_percentage);
}