#ifndef CONFIG_H
#define CONFIG_H

/* Pin definitions */
#define FLASH_CS_PIN        4
#define RF95_CSS_PIN        5
#define BATTERY_PIN         A4
#define INTERRUPT_PIN       7
#define THERMO_CS_PIN       A2

#define SERIAL_TIMEOUT_MS   5000

#define USE_RTC_ZERO        1
#define USE_SAMD_SLEEP_MODE 1
#define USE_RF95            1
#define USE_MOCK_FLASH      0

#endif
