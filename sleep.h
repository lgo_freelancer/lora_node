#ifndef _SLEEP_H
#define _SLEEP_H
#include <stdint.h>

void system_sleep(uint32_t sleep_time_ms);

#endif // _SLEEP_H
