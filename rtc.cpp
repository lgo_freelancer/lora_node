#include "rtc.h"
#include "config.h"

#if USE_RTC_ZERO == 1

#include <RTCZero.h>
RTCZero rtc;

void rtc_begin(void){
    rtc.begin();
}

uint16_t rtc_year(void){
    return rtc.getYear();
}

uint8_t rtc_month(void){
    return rtc.getMonth();
}

uint8_t rtc_day(void){
    return rtc.getDay();
}

uint8_t rtc_hour(void){
    return rtc.getHours();
}

uint8_t rtc_minute(void){
    return rtc.getMinutes();
}

uint8_t rtc_second(void){
    return rtc.getSeconds();
}

void rtc_set(uint32_t t_epoch){
    rtc.setEpoch(t_epoch);
}


#else /* End USE_RTC_ZERO */

#include <TimeLib.h>
/* Functions to use */
void rtc_begin(void){
    setTime(0);
}

uint16_t rtc_year(void){
    return year();
}

uint8_t rtc_month(void){
    return month();
}

uint8_t rtc_day(void){
    return day();
}

uint8_t rtc_hour(void){
    return hour();
}

uint8_t rtc_minute(void){
    return minute();
}

uint8_t rtc_second(void){
    return second();
}

void rtc_set(uint32_t t_epoch){
    setTime(t_epoch);
}

#endif