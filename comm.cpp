#include "comm.h"
#include <Arduino.h>
#include <RHReliableDatagram.h>
#include <RHEncryptedDriver.h>
#include <SPI.h>
#include "config.h"
#include "rtc.h"
#include "flash_simple_api.h"
#include "config_structs.h"

#if defined(ARDUINO_ARCH_SAMD)
extern Uart Serial;
#else
extern HardwareSerial Serial;
#endif

#if USE_RF95 == 1
#include <RH_RF95.h>
RH_RF95 driver(RF95_CSS_PIN, 2);

#else 

#include <RH_NRF24.h>
/* Chip enable - SS */ 
RH_NRF24 driver(8, 9);

#endif

RHReliableDatagram manager(driver, 41);

uint8_t rf_init(void){
    bool res;
    if((res = manager.init()) == true){
        return 0;
    }else{
        return 1;
    }
}

void rf_set_address(uint8_t address){
    manager.setThisAddress(address);
}

void rf_set_frequency(float freq){
  #if USE_RF95 == 1
    driver.setFrequency(freq);
  #else

  #endif
}

void rf_set_power(int8_t pwr){
    #if USE_RF95 == 1
    driver.setTxPower(pwr);
    #else

    #endif
}

void rf_ask_for_config(Node_Internal_Cfg * cfg)
{
  /* Send the initial package to gateway - 1 byte */
  /* Send ACK with package id = 0 */
  uint8_t gateway_id = cfg->gateway_id;
  uint8_t dummy_data[1] = {0xAA};
  Serial.println(F("Sending config request..."));
  if (manager.sendtoWait(dummy_data, sizeof(dummy_data), gateway_id))
  {
    /* ACK received - Wait max 2 seconds for new config. data */
    Node_RF_Cfg config_via_rf;
    uint8_t len = sizeof(config_via_rf);
    uint8_t from;
    /* Try to Receive from gateway a config struct */
    if (manager.recvfromAckTimeout((uint8_t *)&config_via_rf, &len, 2000, &from))
    {
      if (len > 1)
      {
        /* Config Value */
        Serial.print(F("Config data received from ID: "));
        Serial.println(from);
        /* Check that the config is for this node */
        if (from == gateway_id)
        {
          /* Store data to flash */
          Serial.println(F("Data Received..."));
         // config_print_rf(&config_via_rf); /* Print data received */

          /* Update the flash file */
          uint8_t res;
          cfg->node_id = config_via_rf.node_id;
          cfg->gateway_id = config_via_rf.gateway_id;
          cfg->collection_time = config_via_rf.collection_time;
          cfg->power = config_via_rf.power;
          cfg->epoch = config_via_rf.epoch;

          rtc_set(cfg->epoch);

          rf_set_power(cfg->power);

          res = node_internal_config_update(cfg);

          if (res == CFG_OK)
          {
            Serial.println(F("Config file updated correctly"));
          }
          else if (res == CFG_DOESNT_EXIST)
          {
            Serial.print(F("Config file doesn't exist. Halting..."));
            while (1)
              ;
          }
          else
          {
            Serial.print(F("Error updating config. file via RF"));
          }
        }
      }
      else
      {
        /* just an ACK */
        Serial.print(F("Just an ACK received. No config available."));
      }
    }
    else
    {
      Serial.println(F("No config. values have been received."));
      Serial.println(F("Using Existing Configuration"));
    }
  }
  else
  {
    Serial.println(F("No ACK have been received."));
    Serial.println(F("Using Existing Configuration."));
  }
}

uint8_t rf_send_sensor_data(const Sensor_Data * sdata, const Node_Internal_Cfg * cfg){
    Serial.print(F("SEND to ")); Serial.println(sizeof(Sensor_Data));
    bool res;
    uint8_t max_length = driver.maxMessageLength();
    int8_t bytes_left = sizeof(Sensor_Data);
    uint8_t * data_ptr = (uint8_t *)sdata;
    uint8_t tx_len;
    uint8_t mark = 0xA6;
    res = manager.sendtoWait(&mark, 1, cfg->gateway_id);
    if(res != true){
      return 1;
    }

    uint8_t len = 1;
    uint8_t from;
    res = manager.recvfromAckTimeout(&mark, &len, 1000, &from);
    if(res == false){
      return 1;
    }
    if(mark != 0xA6){
      return 1;
    }

    manager.setTimeout(200);
    while(bytes_left > 0){ 
      if(bytes_left >= max_length){
         tx_len = max_length; 
      }else{
        tx_len = bytes_left;
      } 
      Serial.print(F("SEND bytes: ")); Serial.println(tx_len);

      res = manager.sendtoWait(data_ptr, tx_len, cfg->gateway_id);
      if(res == true){
        bytes_left -= tx_len;
        data_ptr += tx_len;
      }else{
        return 1;
      }
    }

    if(bytes_left == 0){
      return 0;
    }else{
      return 1;
    }
}

int16_t rf_rssi(void){
  return driver.lastRssi();
}
void rf_sleep(void){
  driver.sleep();
}

// /* Retuns 0 if AUTH packet is received, 1 otherwise */
// uint8_t rf_ask_for_auth(Node_Internal_Cfg * cfg)
// {
//   const uint8_t gateway_id = cfg->gateway_id;
//   uint8_t packet[14];
//   uint8_t device_id[13];
//   device_id_get(device_id);

//   packet[0] = 0xBB;                  /* Start Marker */
//   memcpy(&packet[1], device_id, 12); /* device_id, 12 bytes */

//   uint8_t rx_packet[25];
//   uint8_t len = sizeof(rx_packet);
//   uint8_t from;

//   Serial.println(F("Sending AUTH request..."));
//   if (manager.sendtoWait(packet, sizeof(packet), gateway_id))
//   {
//     if (manager.recvfromAckTimeout(rx_packet, &len, 2000, &from))
//     {
//       if (from == gateway_id)
//       {

//         /* Check if the returned device_id is the same as mine - 12 bytes */
//         const int res = memcmp(&rx_packet[1], device_id, 12);

//         if (res)
//         {
//           return 1;
//         }

//         /* Get the time in epoch format */
//         uint32_t now;
//         memcpy((uint8_t *)&now, &rx_packet[13], 4);
//         rtc_set(now);

//         Serial.println(F("AUTH OK"));
//         return 0;
//       }
//     }
//   }
//   else
//   {
//     return 1;
//   }
//   return 1;
// }
