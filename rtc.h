#ifndef _RTC_H
#define _RTC_H
#include <stdint.h>

void rtc_begin(void);
uint16_t rtc_year(void);
uint8_t rtc_month(void);
uint8_t rtc_day(void);
uint8_t rtc_hour(void);
uint8_t rtc_minute(void);
uint8_t rtc_second(void);
void rtc_set(uint32_t t_epoch);



#endif // _RTC_H
