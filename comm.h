#ifndef _COMM_H
#define _COMM_H

#include "config_structs.h"
#include <stdint.h>

uint8_t rf_init(void);
void rf_set_frequency(float freq);
void rf_set_power(int8_t pwr);
void rf_set_address(uint8_t address);
int16_t rf_rssi(void);
void rf_sleep(void);
void rf_ask_for_config(Node_Internal_Cfg * cfg);
uint8_t rf_send_sensor_data(const Sensor_Data * sdata, const Node_Internal_Cfg * cfg);

#endif // _COMM_H
