#include "sleep.h"
#include "config.h"
#include <Arduino.h>

#if USE_SAMD_SLEEP_MODE == 1

#include <ArduinoLowPower.h>

void system_sleep(uint32_t sleep_time_ms){
    LowPower.sleep(sleep_time_ms);
}

#else /* End USE_SAMD_SLEEP_MODE */

void system_sleep(uint32_t sleep_time_ms){
    delay(sleep_time_ms);
}

#endif

